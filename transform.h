#include<pcl/registration/transformation_estimation_svd.h>
#include<pcl/registration/transformation_estimation_lm.h>
#include <pcl/sample_consensus/sac_model_registration.h>
#include <pcl/sample_consensus/ransac.h>
 #include <pcl/filters/statistical_outlier_removal.h>
 #include <pcl/filters/extract_indices.h>
#include<vector>

using namespace std;
using namespace Eigen;

inline pcl::PointCloud<pcl::PointXYZ>::Ptr TypeConversion(vector<Vector3f> pt)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ> ());

    int pt_num=pt.size();
    cloud->width = pt_num;
    cloud->height = 1;
    cloud->resize(cloud->width * cloud->height);
    for(int i=0;i<pt_num;i++)
    {
        cloud->points[i].x=pt[i][0];
        cloud->points[i].y=pt[i][1];
        cloud->points[i].z=pt[i][2];
    }
    return cloud;
}

inline void RemoveOutliers(std::vector<std::vector<double> > pts, std::vector<std::vector<double> >& pts_out, std::vector<int >& indices)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    cloud->width=pts.size();
    cloud->height=1;
    cloud->points.resize(cloud->width*cloud->height);
    for(int i=0;i<cloud->points.size();i++)
    {
        cloud->points[i].x=pts[i][0];
        cloud->points[i].y=pts[i][1];
        cloud->points[i].z=pts[i][2];
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor(true);
    sor.setInputCloud (cloud);
    sor.setMeanK (50);
    sor.setStddevMulThresh (1.0);
    sor.filter (*cloud_filtered);
    pts_out.resize(cloud_filtered->points.size());
    for(int i=0;i<cloud_filtered->points.size();i++)
    {
        pts_out[i].resize(3);
        pts_out[i][0]=cloud_filtered->points[i].x;
        pts_out[i][1]=cloud_filtered->points[i].y;
        pts_out[i][2]=cloud_filtered->points[i].z;
    }
    pcl::IndicesConstPtr ind=sor.getRemovedIndices();
    pcl::PointIndices::Ptr removed_indices (new pcl::PointIndices());
    removed_indices->indices = *ind;
    indices.resize(removed_indices->indices.size());
    for(int i=0;i<removed_indices->indices.size();i++)
    {
       indices[i]=removed_indices->indices[i];
    }
 }

inline Matrix4f MyEstimateTransformNoMotion(vector<Vector3f> pt1, vector<Vector3f> pt2)
{
    /*int n=pt1.size();
    vector<vector<double> > ptDiff(n);
    for(int i=0;i<n;i++)
    {
        vector<double> p(3);
        p[0]=pt1[i][0]-pt2[i][0];
        p[1]=pt1[i][1]-pt2[i][1];
        p[2]=pt1[i][2]-pt2[i][2];
        ptDiff[i]=p;
    }
    vector<vector<double> > pt_out;
    vector<int> indices;
    RemoveOutliers(ptDiff,pt_out,indices);
    vector<Vector3f> newPt1, newPt2;
    //cout<<"outliers: "<<indices.size()<<endl;
    int cur=0;
    for(int i=0;i<n;i++)
    {
        if(i==indices[cur])
        {
            cur++;
            continue;
        }
        newPt1.push_back(pt1[i]);
        newPt2.push_back(pt2[i]);
    }
    pt1=newPt1;
    pt2=newPt2;*/

    Vector3f c1(0,0,0), c2(0,0,0);
    int n=pt1.size();
    for(int i=0;i<n;i++)
    {
        Vector3f newC1=c1+pt1[i], newC2=c2+pt2[i];
        c1=newC1;
        c2=newC2;
    }
    Vector3f newC1=c1/n, newC2=c2/n;
    c1=newC1;
    c2=newC2;

    MatrixXf h(3,3);
    for(int i=0;i<n;i++)
    {
        MatrixXf newH=h+(pt1[i]-c1)*(pt2[i]-c2).transpose();
        h=newH;
    }
    JacobiSVD<MatrixXf> svd(h,ComputeThinU|ComputeThinV);
    //cout<<svd.matrixV()<<endl<<svd.matrixU()<<endl;
    MatrixXf r=svd.matrixV()*svd.matrixU().transpose();
    Vector3f t=-r*c1+c2;
    cout<<c1.transpose()<<" "<<c2.transpose()<<" "<<t.transpose()<<endl;
    Matrix4f res=Matrix4f::Identity();
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<4;j++)
        {
            if(j<3)
                res(i,j)=r(i,j);
            else
                res(i,j)=t(i);
        }
    }
    return res;
}

bool mycompare(pair<int,int> i, pair<int, int> j){return i.first>j.first;}

void MyRemoveOutliers(vector<vector<double> > pts,  vector<int>& indices)
{
    const int BINNUM=30;
    int n=pts.size();
    vector<pair<int,int> > bin(BINNUM);
    for(int i=0;i<BINNUM;i++)
    {
        bin[i].first=0;
        bin[i].second=i;
    }
   // vector<int> bin(300);
   // fill(bin.begin(),bin.end(),0);
    for(int i=0;i<n;i++)
    {
        int z=pts[i][2]+BINNUM*10/2;
        if(z>BINNUM*10-1)
            z=BINNUM*10-1;
        else if(z<0)
            z=0;
        z=z/10;
        bin[z].first++;
    }
    sort(bin.begin(), bin.end(), mycompare);
    for(int i=0;i<n;i++)
    {
        int z=pts[i][2]+BINNUM*10/2;
        if(z>BINNUM*10-1)
            z=BINNUM*10-1;
        else if(z<0)
            z=0;
        z=z/10;
        if(z<=0 || z>=BINNUM-1)
        {
            indices.push_back(i);
        }
        if(!(z==bin[0].second || z==bin[1].second || z==bin[2].second || z==bin[3].second || z==bin[4].second))
        {
            indices.push_back(i);
        }
    }

}

inline Matrix4f MyEstimateTransformTranslate(vector<Vector3f> pt1, vector<Vector3f> pt2)
{

    /*int n=pt1.size();
    vector<vector<double> > ptDiff(n);
    for(int i=0;i<n;i++)
    {
        vector<double> p(3);
        p[0]=pt1[i][0]-pt2[i][0];
        p[1]=pt1[i][1]-pt2[i][1];
        p[2]=pt1[i][2]-pt2[i][2];
        ptDiff[i]=p;
    }
    vector<int> indices;
    MyRemoveOutliers(ptDiff, indices);
    cout<<n<<" "<<indices.size()<<endl;
    vector<Vector3f> newPt1, newPt2;
    //cout<<"outliers: "<<indices.size()<<endl;
    indices.push_back(n);
    int cur=0;
    for(int i=0;i<n;i++)
    {
        if(i==indices[cur])
        {
            cur++;
            continue;
        }
        newPt1.push_back(pt1[i]);
        newPt2.push_back(pt2[i]);
    }
    pt1=newPt1;
    pt2=newPt2;*/

    Vector3f c1(0,0,0), c2(0,0,0);
    int n=pt1.size();
    for(int i=0;i<n;i++)
    {
        c1+=pt1[i];
        c2+=pt2[i];
    }
    c1/=n;
    c2/=n;

    Vector3f t=c2-c1;
    cout<<t(2)<<endl;
    Matrix4f res=Matrix4f::Identity();
    for(int i=0;i<3;i++)
    {
        res(i,3)=t(i);
    }
    return res;
}

inline Matrix4f MyEstimateTransform(vector<Vector3f> pt1, vector<Vector3f> pt2)
{
    /*int n=pt1.size();
    vector<vector<double> > ptDiff(n);
    for(int i=0;i<n;i++)
    {
        vector<double> p(3);
        p[0]=pt1[i][0]-pt2[i][0];
        p[1]=pt1[i][1]-pt2[i][1];
        p[2]=pt1[i][2]-pt2[i][2];
        ptDiff[i]=p;
    }
    vector<vector<double> > pt_out;
    vector<int> indices;
    RemoveOutliers(ptDiff,pt_out,indices);
    vector<Vector3f> newPt1, newPt2;
    //cout<<"outliers: "<<indices.size()<<endl;
    int cur=0;
    for(int i=0;i<n;i++)
    {
        if(i==indices[cur])
        {
            cur++;
            continue;
        }
        newPt1.push_back(pt1[i]);
        newPt2.push_back(pt2[i]);
    }
    pt1=newPt1;
    pt2=newPt2;*/

    Vector3f c1(0,0,0), c2(0,0,0);
    int n=pt1.size();
    for(int i=0;i<n;i++)
    {
        c1+=pt1[i];
        c2+=pt2[i];
    }
    c1/=n;
    c2/=n;
    double var=0;
    for(int i=0;i<n;i++)
    {
        var+=(pt1[i]-c1).transpose()*(pt1[i]-c1);
    }
    var/=n;

    MatrixXf h=MatrixXf::Zero(3,3);
    for(int i=0;i<n;i++)
    {
        h=h+(pt2[i]-c2)*(pt1[i]-c1).transpose();
    }
    h=h/n;
    double det=h.determinant();
    MatrixXf S=MatrixXf::Identity(3,3);
    if(det>0)
    {
        S(2,2)=-1;
    }

    JacobiSVD<MatrixXf> svd(h,ComputeThinU|ComputeThinV);
    MatrixXf r=svd.matrixU()*S*svd.matrixV().transpose();
    MatrixXf SV1=svd.singularValues();
    MatrixXf SV=MatrixXf::Zero(3,3);
    for(int i=0;i<3;i++)
        SV(i,i)=SV1(i,0);
    double c=(SV*S).sum()/var;

    Vector3f t=-c*r*c1+c2;
    MatrixXf r2=c*r;
    cout<<c1.transpose()<<endl<<c2.transpose()<<endl<<t.transpose()<<endl<<endl;
    Matrix4f res=Matrix4f::Identity();
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<4;j++)
        {
            if(j<3)
                res(i,j)=r2(i,j);
            else
                res(i,j)=t(i);
        }
    }
   // cout<<c<<endl;
   // cout<<res<<endl;
    return res;
}

inline Matrix4f EstimateTransform(vector<Vector3f> pt1, vector<Vector3f> pt2)
{
    /*int n=pt1.size();
    vector<vector<double> > ptDiff(n);
    for(int i=0;i<n;i++)
    {
        vector<double> p(3);
        p[0]=pt1[i][0]-pt2[i][0];
        p[1]=pt1[i][1]-pt2[i][1];
        p[2]=pt1[i][2]-pt2[i][2];
        ptDiff[i]=p;
    }
    vector<vector<double> > pt_out;
    vector<int> indices;
    RemoveOutliers(ptDiff,pt_out,indices);
    vector<Vector3f> newPt1, newPt2;
    //cout<<"outliers: "<<indices.size()<<endl;
    int cur=0;
    for(int i=0;i<n;i++)
    {
        if(i==indices[cur])
        {
            cur++;
            continue;
        }
        newPt1.push_back(pt1[i]);
        newPt2.push_back(pt2[i]);
    }
    pt1=newPt1;
    pt2=newPt2;*/
   // cout<<"original and left"<<n<<" "<<pt1.size()<<endl;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in=TypeConversion(pt1);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out=TypeConversion(pt2);

    pcl::registration::TransformationEstimationLM<pcl::PointXYZ,pcl::PointXYZ> TESVD;
    pcl::registration::TransformationEstimationLM<pcl::PointXYZ,pcl::PointXYZ>::Matrix4 transformation2;
    TESVD.estimateRigidTransformation (*cloud_in,*cloud_out,transformation2);
    Matrix4f res;
    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)
            res(i,j)=transformation2(i,j);
    //cout<<res<<endl;
    return res;
}

inline Matrix4f RansacRegister(vector<Vector3f> pt1, vector<Vector3f> pt2)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in=TypeConversion(pt1);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out=TypeConversion(pt2);

    pcl::SampleConsensusModelRegistration<pcl::PointXYZ>::Ptr
    sac_model(new pcl::SampleConsensusModelRegistration<pcl::PointXYZ>(cloud_in));
    sac_model->setInputTarget(cloud_out);

    pcl::RandomSampleConsensus<pcl::PointXYZ> ransac(sac_model);
    ransac.setDistanceThreshold(0.1);

    ransac.computeModel(1);

    Eigen::VectorXf coeffs;
    ransac.getModelCoefficients(coeffs);

    if(coeffs.size()!=16)
        return Matrix4f::Identity();
    //assert(coeffs.size() == 16);
    Matrix4f res = Eigen::Map<Eigen::Matrix4f>(coeffs.data(),4,4);
    Matrix4f res2=res.transpose();
    cout<<res2<<endl;
    return res2;
}

#include<iostream>
#include <revlib.h>
#include<fstream>
#include <eigen3/Eigen/Dense>
#include "cmdline.h"
#include<string>
#include<cmath>

using namespace std;
using namespace Eigen;

float baseline, baseline_ratio;
float fx, fy, ox, oy;
const float DISPARITY_FACTOR=256;

MatrixXf disparity1, disparity2, disparity3, horizontalFlow, verticalFlow;

string image1_name, image2_name, image3_name, flow1_name, flow2_name, out_name;

void ParseArguments(int argc, char* argv[])
{
    cmdline::parser commandParser;
    commandParser.add<double>("baseline", 'b', "baseline value", false, 0.53084992269);
    commandParser.add<double>("baseline_ratio", 'r', "baseline ratio value", false, 0.9712869744);
    commandParser.add<double>("fx", 'x', "fx value", false, 999.7838133341337);
    commandParser.add<double>("fy", 'y', "fy value", false, 999.7838133341337);
    commandParser.add<double>("ox", 'o', "ox value", false, 792.9412307739258);
    commandParser.add<double>("oy", 'p', "oy value", false, 609.6846313476562);
    commandParser.add("help", 'h', "display this message");

    commandParser.set_program_name("floweval");
    commandParser.footer("image1, image2, image3, flow1, flow2, outname");
    bool isCorrectCommandline = commandParser.parse(argc, argv);
	// Check arguments
	if (!isCorrectCommandline) {
		std::cerr << commandParser.error() << std::endl;
	}
	if (!isCorrectCommandline || commandParser.exist("help") || commandParser.rest().size() < 5) {
		std::cerr << commandParser.usage() << std::endl;
		exit(1);
	}

	image1_name=commandParser.rest()[0];
	image2_name=commandParser.rest()[1];
	image3_name=commandParser.rest()[2];
	flow1_name=commandParser.rest()[3];
	flow2_name=commandParser.rest()[4];
	out_name=commandParser.rest()[5];

    baseline=commandParser.get<double>("baseline");
    baseline_ratio=commandParser.get<double>("baseline_ratio");
    fx=commandParser.get<double>("fx");
    fy=commandParser.get<double>("fy");
    ox=commandParser.get<double>("ox");
    oy=commandParser.get<double>("oy");
}

float GetDepth(MatrixXf disparity, int image_x, int image_y)
{
     float cur_depth=(baseline/baseline_ratio)*fx/(disparity(image_y,image_x)/DISPARITY_FACTOR);
     return cur_depth;
}

MatrixXf LoadDisparityImage(const char* disparityfile)
{
    rev::Image<unsigned short> disparityImage;
    disparityImage = rev::read16bitImageFile(disparityfile);

    MatrixXf disparity(disparityImage.height(), disparityImage.width());
    for (int u = 0; u < disparityImage.height(); u++)
        for (int v = 0; v < disparityImage.width(); v++)
            disparity(u, v) = disparityImage(v, u);

    return disparity;
}

MatrixXf LoadOpticalFlow(const char* filename, int height, int width)
{
    ifstream myfile(filename);
    MatrixXf flow(height, width);

    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            float tmp;
            myfile>>tmp;
            flow(h,w)=tmp;
        }
    }
    myfile.close();
    return flow;
}

void LoadData()
{
    disparity1=LoadDisparityImage(image1_name.c_str());
    disparity2=LoadDisparityImage(image2_name.c_str());
    disparity3=LoadDisparityImage(image3_name.c_str());
    int height=disparity1.rows(), width=disparity1.cols();

    horizontalFlow=LoadOpticalFlow(flow1_name.c_str(),height,width);
    verticalFlow=LoadOpticalFlow(flow2_name.c_str(),height,width);
}

MatrixXf GetNewDisparityMap()
{
    int height=disparity1.rows(), width=disparity1.cols();
    //double b=baseline/baseline_ratio;
    double b=baseline;

    MatrixXf newDisparity=MatrixXf::Zero(height,width);
    int total=0;
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            float d1=disparity1(h,w)/DISPARITY_FACTOR;
            float x=b*(w-ox)/d1, y=b*fx*(h-oy)/(fy*d1), z=b*fx/d1;
            float newW=w+horizontalFlow(h,w), newH=h+verticalFlow(h,w);

            if(newW<0 || newW>=width-1 || newH<0 || newH>=height-1)
                continue;

            int fh=floor(newH),fw=floor(newW),ch=ceil(newH),cw=ceil(newW);
            if(ch==newH)
                ch++;
            if(cw==newW)
                cw++;
            float d00=disparity2(fh,fw)/DISPARITY_FACTOR,
            d01=disparity2(fh,cw)/DISPARITY_FACTOR,
            d10=disparity2(ch,fw)/DISPARITY_FACTOR,
            d11=disparity2(ch,cw)/DISPARITY_FACTOR;
             float d2=d00*(ch-newH)*(cw-newW)+d01*(ch-newH)*(newW-fw)
            +d10*(newH-fh)*(cw-newW)+d11*(newH-fh)*(newW-fw);
           // int newW=nearbyint(w+horizontalFlow(h,w)), newH=nearbyint(h+verticalFlow(h,w));
           // if(newW<0 || newW>=width || newH<0 || newH>=height)
          //      continue;

        //    float d2=disparity2(newH,newW)/DISPARITY_FACTOR;
            float newX=b*(newW-ox)/d2, newY=b*fx*(newH-oy)/(fy*d2), newZ=b*fx/d2;

             float newW2=w+3*horizontalFlow(h,w), newH2=h+3*verticalFlow(h,w);
              if(newW2<0 || newW2>=width-1 || newH2<0 || newH2>=height-1)
                continue;

            fh=floor(newH2);
            fw=floor(newW2);
            ch=ceil(newH2);
            cw=ceil(newW2);
            if(ch==newH2)
                ch++;
            if(cw==newW2)
                cw++;
            d00=disparity3(fh,fw)/DISPARITY_FACTOR;
            d01=disparity3(fh,cw)/DISPARITY_FACTOR;
            d10=disparity3(ch,fw)/DISPARITY_FACTOR;
            d11=disparity3(ch,cw)/DISPARITY_FACTOR;
            d2=d00*(ch-newH2)*(cw-newW2)+d01*(ch-newH2)*(newW2-fw)
            +d10*(newH2-fh)*(cw-newW2)+d11*(newH2-fh)*(newW2-fw);
             float newX2=b*(newW2-ox)/d2, newY2=b*fx*(newH2-oy)/(fy*d2), newZ2=b*fx/d2;


            //  float preX=newX2, preY=newY2, preZ=newZ2;
            float preX=newX2+(newX-x), preY=newY2+(newY-y), preZ=newZ2+(newZ-z);
            int preiX=nearbyint(fx*preX/preZ+ox), preiY=nearbyint(fy*preY/preZ+oy);
            float estD=b*fx/preZ;
            if(preiY>=0 && preiY<height && preiX>=0 && preiX<width)
            {
                newDisparity(preiY,preiX)=estD*DISPARITY_FACTOR;
            }
        }
    }
    return newDisparity;
}

void SaveNewDisparity(MatrixXf newDisparity)
{
    int height=newDisparity.rows(), width=newDisparity.cols();
    rev::Image<unsigned short> disparityImage(width,height,1);
    for(int h=0;h<height;h++)
    {
        for(int w=0;w<width;w++)
        {
            disparityImage(w,h)=newDisparity(h,w);
        }
    }
    write16bitImageFile(out_name, disparityImage);
}

int main(int argc, char** argv)
{
    ParseArguments(argc, argv);
    LoadData();
    MatrixXf newDisparity=GetNewDisparityMap();
    SaveNewDisparity(newDisparity);
}

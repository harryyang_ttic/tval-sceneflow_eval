#ifndef REVLIB_RANDOM_NUMBER_GENERATOR_H
#define REVLIB_RANDOM_NUMBER_GENERATOR_H

namespace rev {
    
class RandomNumberGenerator {
public:
    RandomNumberGenerator() {
        generatorValue_ = static_cast<unsigned long long>(-1);
    }
    RandomNumberGenerator(const unsigned long long seed) {
        if (seed != 0) generatorValue_ = seed;
        else generatorValue_ = static_cast<unsigned long long>(-1);
    }
    
    unsigned int randomInteger() {
        const unsigned long long generatorCoefficient = 4164903690U;

        generatorValue_ = static_cast<unsigned long long>(static_cast<unsigned int>(generatorValue_)*generatorCoefficient
                                                          + (generatorValue_ >> 32));
        
        return static_cast<unsigned int>(generatorValue_);
        
    }
    double randomDouble() {
        return randomInteger()*2.3283064365386962890625e-10;
    }
    
private:
    unsigned long long generatorValue_;
};
    
}

#endif
